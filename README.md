# StreetLife Test Mobile Application

This is a test application for StreetLife.

### Prerequisites

Follwing are needed before running the app

* [NPM](https://www.npmjs.com/) 
* [React Native](https://facebook.github.io/react-native/) 


### Installing

Following are steps for installation

Goto the root directory of the project, 

```
npm install
```

After that run the link command

```
react-native link
```

### Google Map
To use google map please follow steps for installation

* [react-native-maps](https://github.com/react-community/react-native-maps/blob/master/docs/installation.md) - React Native nap installation


### Image Picker
For image picker please follow steps in the link 

* [react-native-image-picker](https://github.com/react-community/react-native-image-picker/blob/master/docs/Install.md) - Image picker installation

