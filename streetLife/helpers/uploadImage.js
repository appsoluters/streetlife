import RNFetchBlob from 'react-native-fetch-blob';
import DetailsStyle from '../assets/styles/details';
import ImagePicker from 'react-native-image-picker';
import {locationStorageRef} from '../firebase';
import {
  Platform
} from 'react-native';

const options = {
  title: 'Select Image',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};


export default uploadImage =  (uri, mime = 'application/octet-stream') => {
  const Blob = RNFetchBlob.polyfill.Blob;
  const fs = RNFetchBlob.fs;
  window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest;
  window.Blob = Blob;
     return new Promise((resolve, reject) => {
       const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri;
       let uploadBlob = null;
       let imageName = '_' + Math.random().toString(36).substr(2, 9);
       const imageRef = locationStorageRef.child(imageName + '.jpg');
       let mime = 'image/jpg';
       fs.readFile(uploadUri, 'base64')
         .then((data) => {
           return Blob.build(data, {type: `${mime};BASE64`})
         }).then((blob) => {
           uploadBlob = blob;
           return imageRef.put(blob, {contentType: mime});
         }).then(() => {
           uploadBlob.close;
           return imageRef.getDownloadURL()
         }).then((url) => {
            resolve(url)
          })
          .catch((error) => {
            reject(error)
          })
     })

}
