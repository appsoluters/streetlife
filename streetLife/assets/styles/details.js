import { StyleSheet, Dimensions } from 'react-native';


export default MapStyle = StyleSheet.create({

  screenContainer: {
    backgroundColor: 'black',
    paddingHorizontal: 10
  },
  ImageSection:{
    height: 300,
  },
  topImage: {
    flex: 3,
    width: "100%",
    height: "100%",
    alignSelf: "center",
    backgroundColor: "#FFF",
  },
  InfoSection:{
    height: 60,
    backgroundColor: '#FFF',
    paddingVertical: 10,
    paddingHorizontal: 10
  },
  InfoSectionRow:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom:5
  },
  InfoSectionRowLeft:{
    flexDirection: 'row',
    alignItems: 'center',
    flex:2
  },
  InfoSectionRowRight:{
    flexDirection: 'row',
    alignItems: 'center',
    flex:1
  },
  InfoSectionRowIcon:{
    marginRight: 5
  },
  InfoSectionRowText:{
    fontSize: 12
  },
  VisitorsSection:{
    backgroundColor:'#FFF',
    flexDirection: 'row',
    paddingHorizontal: 10,
    paddingBottom: 10
  },
  VisitorsSectionLeft:{
    flex:1,
    justifyContent: 'center'
  },
  VisitorsSectionLeftText:{
    fontSize: 14,
    fontWeight: 'bold'
  },
  VisitorsSectionRight:{
    flex:1,
    flexDirection: 'row'
  },
  VisitorsAvators: {
    width: 30,
    height: 30,
    marginRight: -10
  },
  VisitorsAvatorsImage: {
    width: "100%",
    height: "100%",
    alignSelf: "center",
    borderRadius: 15,
    borderColor: 'white',
    borderWidth: 2,
  },
  DescSection:{
    height: 200,
    backgroundColor: "#FFF",
    paddingVertical: 10,
    paddingHorizontal: 10
  },
  DescSectionTitle:{
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 10
  },
  DescSectionText:{
    fontSize: 12
  },
  MediaSection:{
    height: 150,
    backgroundColor: '#FFF',
    paddingHorizontal: 10,
  },
  MediaSectionTitle:{
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 10
  },
  MediaSectionImagesView:{
    flexDirection: 'row',
  },
  MediaSectionNoImagesView:{
    flex:1,
    height:'100%',
    justifyContent:'center',
    alignItems:'center',
    padding: 20
  },
  MediaSectionNoImagesText:{
    fontSize: 14,
    textAlign: 'center'
  },
  MediaSectionImageBox: {
    width: 100,
    height: 100,
    marginRight: 10
  },
  MediaSectionImage: {
    width: "100%",
    height: "100%",
    alignSelf: "center",
  },
  ButtonsSection:{
    height: 100,
    backgroundColor: '#FFF',
    justifyContent: 'center',
    alignItems: 'center'
  },
  UploadButton:{
    width: 200,
    height: 50,
    backgroundColor: 'rgba(255, 215, 0, 1)',
    justifyContent: 'center',
    alignItems: 'center'
  },
  UploadButtonText:{
    fontSize: 14,
    fontWeight: 'bold'
  }
});
