import { StyleSheet, Dimensions } from 'react-native';


export default MapStyle = StyleSheet.create({

  screenContainer: {
    flex: 1,
  },
  scrollView: {
    position: "absolute",
    bottom: 30,
    left: 0,
    right: 0,
    paddingVertical: 10,
  },
  card: {
    elevation: 2,
    shadowColor: "#000",
    shadowRadius: 5,
    shadowOpacity: 0.3,
    shadowOffset: { x: 2, y: -2 },
    height: 180,
    width: "100%",
    overflow: "hidden"
  },
  selectedCard: {
    borderBottomWidth: 3,
    borderBottomColor: 'rgba(255, 165, 0, 1)'
  },
  cardImage: {
    flex: 3,
    width: "100%",
    height: "100%",
    alignSelf: "center",
    marginLeft: 10,
    backgroundColor: "#FFF",
  },
  cardCategory: {
    marginTop: 10,
    position: 'absolute',
    flexDirection: 'row'
  },
  cardCategoryText: {
    fontSize: 12,
    color: 'white',
    paddingVertical: 8,
    paddingHorizontal: 25,
    textAlign: 'center',
    fontWeight: 'bold',
    backgroundColor: 'black',
  },
  cardAvatorsView: {
    paddingLeft: 20,
    width: 120,
    flexDirection: 'row',
    alignItems: 'center'
  },
  cardAvators: {
    width: 30,
    height: 30,
    marginRight: -10
  },
  cardAvatorsImage: {
    width: "100%",
    height: "100%",
    alignSelf: "center",
    borderRadius: 15,
    borderColor: 'white',
    borderWidth: 2,
  },
  textContent: {
    flex: 1,
    marginLeft: 5,
    backgroundColor: "#FFF",
    flexDirection: 'row',
  },
  cardInfoView: {
    flex: 3,
    paddingLeft: 10,
    paddingTop: 10,
  },
  cardIconView: {
    flex: 1,
    width: 20,
    justifyContent: 'center',
    alignItems: 'center'
  },
  cardIcon: {
    color: 'black',
  },
  cardTitle: {
    fontSize: 12,
    fontWeight: "bold",
  },
  cardInfoText: {
    fontSize: 10,
    marginTop: 5,
    marginLeft: 5
  },
  mapContainer: {
    flex: 1
  },
  markerWrap: {
    width: 40,
    height: 40,
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
  },
  marker: {
    width: 6,
    height: 6,
    borderRadius: 20,
    backgroundColor: "rgba(0, 204, 255, 0.9)",
  },
  ring: {
    width: 40,
    height: 40,
    borderRadius: 20,
    backgroundColor: "rgba(0, 204, 255, 0.09)",
    position: "absolute",
  },

  markerWrapSelected: {
    width: 30,
    height: 30,
    borderRadius: 15,
    alignItems: "center",
    justifyContent: "center",
  },
  markerSelected: {
    width: 4,
    height: 4,
    borderRadius: 2,
    backgroundColor: "rgba(255, 255, 255, 1)",
  },
  firstRingSelected: {
    width: 12,
    height: 12,
    borderRadius: 6,
    backgroundColor: "rgba(255, 215, 0, 0.2)",
    position: "absolute",
    borderColor: "rgba(255,215,0, 1)",
    borderWidth: 1
  },
  secondRingSelected: {
    width: 20,
    height: 20,
    borderRadius: 10,
    backgroundColor: "rgba(255, 215, 0, 0.3)",
    position: "absolute",
  },
  ringSelected: {
    width: 30,
    height: 30,
    borderRadius: 15,
    backgroundColor: "rgba(255, 215, 0, 0.2)",
    position: "absolute",
  },

});
