const BASE_URL = "https://streetlife.herokuapp.com/api";
const LOCATION_URL = BASE_URL + "/locations";
const UPLOAD_MEDIA = LOCATION_URL + "/uploadMedia";
import {ApiUtils} from './ApiUtils';

class ServerApi {

   getAllLocations() {
     return fetch(LOCATION_URL,{
           method: 'GET',
           headers: {
             'Content-Type': 'application/json',
           }
       })
       .then(ApiUtils.checkStatus)
       .then(response => response.json())
   }

   uploadMedia(locationId, path, type){
     let requestParams ={
      "locationId": locationId,
      "path": path,
      "type": type,
      }
      return fetch(UPLOAD_MEDIA,{
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify(requestParams)
        })
        .then(ApiUtils.checkStatus)
        .then(response => response.json())
   }
}

export default new ServerApi();
