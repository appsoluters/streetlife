export const ApiUtils = {
  checkStatus: function(response) {
    //console.log("apiutls");
    //console.log(response);
    if (response.ok) {
      return response;
    } else {
       let error_message = "Some error in the server";
       if(response.status === 401){
         error_message = "You are not authorized";
       }
       //throw "There is some error in server."
       let error = new Error(error_message);
       throw error;
    }
  }
};
