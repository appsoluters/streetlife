import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  TouchableOpacity
} from 'react-native';
import MapStyle from '../assets/styles/map';
import { Actions } from 'react-native-router-flux';

import FontAwesome from 'react-native-vector-icons/FontAwesome'

export default SliderItem = ({marker, selectedId}) => {

  let textContentClasses = [MapStyle.textContent]

  if(marker._id === selectedId )
    textContentClasses.push(MapStyle.selectedCard);
    
  return (
      <View style={MapStyle.card} >
          <TouchableOpacity style={{flex:2}} onPress={()=>Actions.details({location: marker})}>
            <Image
                source={{uri: marker.image}}
                style={MapStyle.cardImage}
                resizeMode="cover"
             />
          </TouchableOpacity>

           <View style={MapStyle.cardCategory}>
            <Text style={MapStyle.cardCategoryText}>{marker.category}</Text>
            <View style={MapStyle.cardAvatorsView}>
              <View style={MapStyle.cardAvators}>
                <Image
                  source={require('../assets/images/user.jpeg')}
                  style={MapStyle.cardAvatorsImage}
                  resizeMode="cover"
                />
              </View>
              <View style={MapStyle.cardAvators}>
              <Image
                source={require('../assets/images/user2.jpeg')}
                style={MapStyle.cardAvatorsImage}
                resizeMode="cover"
              />
              </View>
            </View>
           </View>
           <View style={textContentClasses}>
              <View style={MapStyle.cardInfoView}>
                <Text numberOfLines={1} style={MapStyle.cardTitle}>{marker.name}</Text>
                <Text numberOfLines={1} style={MapStyle.cardInfoText}>11 km . FREE</Text>
              </View>

              <TouchableOpacity onPress={() => alert("this open menu to show different modes for distance calculation")} style={MapStyle.cardIconView}>
                <FontAwesome name="automobile" size={12} style={MapStyle.cardIcon}/>
              </TouchableOpacity>

          </View>
       </View>
  );
}
