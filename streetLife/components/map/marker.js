import React from 'react';
import {
  View
} from 'react-native';

import { Marker } from 'react-native-maps';
import MapStyle from '../../assets/styles/map';

export default MapMaker = ({marker, selectedId}) => {

  return (
    <Marker
      key={marker._id}
      coordinate={marker.coordinate}
    >
     {
       (selectedId == marker._id) ?
           <View style={MapStyle.markerWrapSelected}>
             <View style={MapStyle.ringSelected} />
             <View style={MapStyle.firstRingSelected} />
             <View style={MapStyle.secondRingSelected} />
             <View style={MapStyle.markerSelected} />
           </View>
        :
          <View style={MapStyle.markerWrap}>
            <View style={MapStyle.ring} />
            <View style={MapStyle.marker} />
          </View>
     }

    </Marker>
  )

}
