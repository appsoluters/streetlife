import React, { Component } from 'react';
import { Router, Scene, Actions } from 'react-native-router-flux';

import LoadingScreen from '../views/loading';
import MapScreen from '../views/map';
import DetailsScreen from '../views/details';

const Root = (props) => {

   return (
     <Router>

      <Scene key="root">

        <Scene key="loading"
          component={LoadingScreen}
          title=""
          hideNavBar={true}
          initial
        />

        <Scene key="map"
          component={MapScreen}
          title=""
          hideNavBar={true}

        />

        <Scene key="details"
          component={DetailsScreen}
          title=""
          hideNavBar={true}

        />

      </Scene>
     </Router>
   );
}

export default Root;
