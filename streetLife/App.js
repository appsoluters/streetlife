/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';

import Root from './config/routes';
import { StatusBar } from 'react-native';
export default class App extends Component{

  componentDidMount() {
       StatusBar.setHidden(true);
  }

  render() {
    return (<Root />);
  }
}
