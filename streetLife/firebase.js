import { initializeApp } from 'firebase'

export const fireApp = initializeApp({
  apiKey: "AIzaSyBMdegtG5j1Z7YetQUGN5RsisgD6N7prJQ",
  storageBucket: "streetlife-67b15.appspot.com"
})

// storage refs
export const locationStorageRef = fireApp.storage().ref('locations')
