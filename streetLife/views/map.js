import React, { Component } from 'react';
import {
  Text,
  View,
  Animated,
  ScrollView,
  Dimensions,
  Image
} from 'react-native';
const { width, height } = Dimensions.get("window");

import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';

import MapStyle from '../assets/styles/map';
import Carousel from 'react-native-snap-carousel';
import MapMaker from '../components/map/marker';
import SliderItem from '../components/sliderItem';
import {GOOGLE_DIRECTION_API_KEY} from '../config/constants';

const mapCustomeStyle = require('../assets/styles/mapType.json');

const LATITUDE_DETLA = 0.09464195044303443;
const LOGITUDE_DELTA =  0.088142817690068;

export default class Map extends Component {

  constructor(props){
    super(props);
    this.state = {
      selectedMarkerId: "", //default selected marker id
      markers: [],
      directionOrigin: {},
      directionDestination: {},
      initialRegion: {}
    }
  }

  componentWillMount() {
    this.setState({
      markers: this.props.markers,
      selectedMarkerId: this.props.selectedMarkerId,
      position: this.props.position
    },
     () => {
       this._directionInitialRoute();
     })

  }

  _directionInitialRoute = () => {
    const { selectedMarkerId, markers, position } = this.state
    const destination = markers.find( marker => marker._id === selectedMarkerId );

    this.setState({
      directionOrigin:{
        latitude: position.coords.latitude,
        longitude: position.coords.longitude,
      },
      initialRegion:{
        latitude: position.coords.latitude,
        longitude: position.coords.longitude,
        latitudeDelta: LATITUDE_DETLA,
        longitudeDelta: LOGITUDE_DELTA
      },
      directionDestination:{
        latitude: destination.coordinate.latitude,
        longitude: destination.coordinate.longitude,
      }
    })
  }

  _centerMapOnMarker (marker) {
        const {markers} = this.state;
        const mapRef = this.map;
        const markerData = markers[marker];

        this.setState({
          selectedMarkerId: markerData._id
        })

        if (!markerData || !mapRef) {
            return;
        }
        mapRef.animateToRegion({
            latitude: markerData.coordinate.latitude,
            longitude: markerData.coordinate.longitude,
            latitudeDelta: LATITUDE_DETLA,
            longitudeDelta: LOGITUDE_DELTA
        });
        this.setState({
          directionDestination:{
            latitude: markerData.coordinate.latitude,
            longitude: markerData.coordinate.longitude,
          }
        })
    }
  _renderSliderItem = ({item, index}) => {
     return (
       <SliderItem marker={item} selectedId={this.state.selectedMarkerId}/>
     )
  }

  render(){
    const { markers, initialRegion, selectedMarkerId, directionOrigin, directionDestination } = this.state;
    return(
      <View style={MapStyle.screenContainer}>
        <MapView
            ref={map => { this.map = map }}
            provider={ PROVIDER_GOOGLE }
            style={ MapStyle.mapContainer }
            customMapStyle = {mapCustomeStyle}
            showsUserLocation={true}
            initialRegion={ initialRegion }
          >
           {markers.map(marker => (
              <MapMaker marker={marker} selectedId={selectedMarkerId} />
           ))}

           <MapViewDirections
              origin={directionOrigin}
              destination={directionDestination}
              apikey= {GOOGLE_DIRECTION_API_KEY}
              strokeWidth={3}
              strokeColor="white"
              mode = "driving"
            />

          </MapView>

        <Carousel
              containerCustomStyle={MapStyle.scrollView}
              ref={(c) => { this._carousel = c; }}
              data={markers}
              renderItem={this._renderSliderItem}
              sliderWidth={width}
              itemWidth={width - 150}
              activeSlideAlignment="start"
              inactiveSlideScale={1}
              inactiveSlideOpacity={0.8}
              removeClippedSubviews={false}
              snapOnAndroid={true}
              showsHorizontalScrollIndicator={false}
              slideStyle={{marginRight:20}}
              onSnapToItem={(index) => this._centerMapOnMarker(index)}
              useNativeDriver={true}
            />

      </View>
    )
  }

}
