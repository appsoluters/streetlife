import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ActivityIndicator,
  StyleSheet
} from 'react-native';

import { Actions } from 'react-native-router-flux';

import ServerApi from '../models/serverApi';

export default class Loading extends Component {

  constructor(props){
    super(props);
    this.state = {
      isLoading:true,
      serverError:''
    }
  }

  componentWillMount(){
    navigator.geolocation.getCurrentPosition(
      (position) => {
        this.getLocationsFromServer(position)
      },
      (error) => this.setState({ error: error.message }),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
    );
  }

  getLocationsFromServer = (position) => {
    ServerApi.getAllLocations()
             .then(response => {
               Actions.map({
                 markers: response,
                 selectedMarkerId: response[0]._id,
                 position: position
               })
             })
             .catch(error => {
               this.setState({
                 isLoading: false,
                 serverError: error.message
               })
             })
  }

  render(){
    const {isLoading,serverError} = this.state;
    return (
        <View style={styles.container} >
            <View style={styles.loaderSection}>
            {
              isLoading?
                <View>
                 <ActivityIndicator size="large" color="rgba(255, 215, 0, 1)"/>
                 <Text style={styles.infoSection}>Loading ...</Text>
                </View>
              :
                (serverError!='')?
                  <Text style={styles.errorSection}>{serverError}</Text>
                :
                null
            }
            </View>
          </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  loaderSection:{
    flex:1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  infoSection:{
    fontSize: 12,
    marginTop: 10
  },
  errorSection:{
    fontSize: 12,
    color: 'red'
  }
});
