import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
  ActivityIndicator
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';

import DetailsStyle from '../assets/styles/details';
import ImagePicker from 'react-native-image-picker';
import ServerApi from '../models/serverApi';
import uploadImage from '../helpers/uploadImage';
import PopupDialog, {
  DialogTitle,
  DialogButton,
  SlideAnimation,
} from 'react-native-popup-dialog';

const slideAnimation = new SlideAnimation({ slideFrom: 'right' });

const locations = {
  _id: '5bd5f74ffb6fc074abb14f58',
  coordinate: {
    latitude: 25.121312,
    longitude: 55.199608,
  },
  title: "Best Place",
  description: "The aptly-named Kite Beach is the perfect spot to test out your watersports skills, but there's plenty to do out of the water at this popular spot, too! Grab a smoothie from one of the many open-air cafes and restaurants just steps from the beach, watch bikers and skaters fly by at the newly finished skate park or get into a game of volleyball. With plenty of beach activities, plus an awesome view of the Burj Al Arab, it's easy to spend the whole day here.",
  category: "Beach",
  address: "Umm Suqeim - Dubai",
  image: "https://www.dubaievisa.in/uploads/del_image/thumb_del_1510838571.jpg",
  media: [
  ]
}

const options = {
  title: 'Select Image',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

export default class Details extends Component {

  constructor(props){
      super(props);
      this.state = {
        imageUploading: false,
        imageUploadingMessage: 'Uploading image...',
        imageUploadingError: false,
        imageUploadingErrorMessage: '',
        location: {}
      }
  }

  componentWillMount(){
    this.setState({
      location: this.props.location
    })
  }

  _renderPopupView = () =>{

    const {imageUploading, imageUploadingError, imageUploadingMessage, imageUploadingErrorMessage} = this.state;

    if(imageUploading){
      return(
        <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
          <ActivityIndicator size="large" color="rgba(255, 215, 0, 1)"/>
          <Text style={{marginTop:10}}>{imageUploadingMessage}</Text>
        </View>
      )
    }else{
      if(imageUploadingError){
        return(
          <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
            <Text style={{marginTop:10}}>{imageUploadingErrorMessage}</Text>
            <View style={DetailsStyle.ButtonsSection}>
              <TouchableOpacity onPress={() => this.loadingPopup.dismiss()} style={DetailsStyle.UploadButton}>
                <Text style={DetailsStyle.UploadButtonText}>Close</Text>
              </TouchableOpacity>
            </View>
          </View>
        )
      }
    }
  }

  _beforeImageUploadState = (imageUploading, imageUploadingMessage) => {
    this.setState({
        imageUploading,
        imageUploadingMessage
    })
  }

  _afterImageUploadState = (imageUploadingError = false, imageUploadingErrorMessage = '') => {
    this.setState({
        imageUploading: false,
        imageUploadingMessage: '',
        imageUploadingError,
        imageUploadingErrorMessage
    })
  }

  _uploadImage = () => {
    const { location } = this.state;

    ImagePicker.showImagePicker(options, async (response) => {
      if (response.didCancel) {
        return false;
      } else if (response.error) {
        this.loadingPopup.show();
        this._afterImageUploadState(true, response.error)
      } else if (response.customButton) {
        return false;
      } else {
        this._beforeImageUploadState(true, 'Uploading image...');
        this.loadingPopup.show();
        try {
          let imageUrl = await uploadImage(response.uri);
          this.setState({imageUploadingMessage: 'Updating Server...'});
          let serverResponse = await ServerApi.uploadMedia(location._id, imageUrl, "image");
          this.setState({location: serverResponse})
          this._afterImageUploadState(false, '')
          this.loadingPopup.dismiss();
        } catch (error) {
          this._afterImageUploadState(true, error.message);
        }
      }

    });
  }

  _renderRecentImage = () => {
    const { location } = this.state;
    if(!location.hasOwnProperty("media") || location.media.length <= 0) {
      return (
        <View style={DetailsStyle.MediaSectionNoImagesView}>
          <Text style={DetailsStyle.MediaSectionNoImagesText}>Hey, no one has visited this location yet. Be the first one to upload an image</Text>
        </View>
      )
    }else{
      return (
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          {
            location.media.map((src => (
            <View style={DetailsStyle.MediaSectionImageBox}>
              <Image
                  source={{uri: src.path}}
                  style={DetailsStyle.MediaSectionImage}
                  resizeMode="cover"
               />
            </View>
            )))
          }
        </ScrollView>
      )

    }

  }

  render(){
    const { location } = this.state;
    return(
      <View style={DetailsStyle.screenContainer}>

        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={DetailsStyle.ImageSection}>
            <Image
                source={{uri: location.image}}
                style={DetailsStyle.topImage}
                resizeMode="cover"
             />
            <TouchableOpacity onPress={() => Actions.pop()} style={{ position: 'absolute', top:10, left: 10}}>
              <Ionicons name="md-arrow-back" size={24} style={{color: "#FFF"}} />
            </TouchableOpacity>
          </View>
          <View style={DetailsStyle.InfoSection}>
            <View style={DetailsStyle.InfoSectionRow}>
                <View style={DetailsStyle.InfoSectionRowLeft}>
                  <Entypo name="location-pin" size={18} style={DetailsStyle.InfoSectionRowIcon}/>
                  <Text style={DetailsStyle.InfoSectionRowText}>{location.address}</Text>
                </View>
                <View style={DetailsStyle.InfoSectionRowRight}>
                  <Entypo name="compass" size={18} style={DetailsStyle.InfoSectionRowIcon}/>
                  <Text style={DetailsStyle.InfoSectionRowText}>distance</Text>
                </View>
            </View>
            <View style={DetailsStyle.InfoSectionRow}>
                <View style={DetailsStyle.InfoSectionRowLeft}>
                  <Entypo name="500px-with-circle" size={18} style={DetailsStyle.InfoSectionRowIcon}/>
                  <Text style={DetailsStyle.InfoSectionRowText}>Price</Text>
                </View>
                <View style={DetailsStyle.InfoSectionRowRight}>
                  <Entypo name="back-in-time" size={18} style={DetailsStyle.InfoSectionRowIcon}/>
                  <Text style={DetailsStyle.InfoSectionRowText}>location</Text>
                </View>
            </View>
          </View>

          <View style={DetailsStyle.VisitorsSection}>
            <View style={DetailsStyle.VisitorsSectionLeft}>
              <Text style={DetailsStyle.VisitorsSectionLeftText}>Recent Visitors</Text>
            </View>
            <View style={DetailsStyle.VisitorsSectionRight}>
              <View style={DetailsStyle.VisitorsAvators}>
                <Image
                  source={require('../assets/images/user.jpeg')}
                  style={DetailsStyle.VisitorsAvatorsImage}
                  resizeMode="cover"
                />
              </View>
              <View style={DetailsStyle.VisitorsAvators}>
              <Image
                source={require('../assets/images/user2.jpeg')}
                style={DetailsStyle.VisitorsAvatorsImage}
                resizeMode="cover"
              />
              </View>
            </View>

          </View>

          <View style={DetailsStyle.DescSection}>
            <Text style={DetailsStyle.DescSectionTitle}>About Location</Text>
            <Text style={DetailsStyle.DescSectionText}>{location.description}</Text>
          </View>

          <View style={DetailsStyle.MediaSection}>
            <Text style={DetailsStyle.MediaSectionTitle}>Recently Added Pictures</Text>
            <View style={DetailsStyle.MediaSectionImagesView}>
                {this._renderRecentImage()}
            </View>
          </View>

          <View style={DetailsStyle.ButtonsSection}>
            <TouchableOpacity onPress={this._uploadImage} style={DetailsStyle.UploadButton}>
              <Text style={DetailsStyle.UploadButtonText}>Upload Image</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>

        <PopupDialog
          ref={(popupDialog) => { this.loadingPopup = popupDialog; }}
          dialogAnimation={slideAnimation}
          dialogStyle={{bottom:'10%',padding:20}}
          width={300}
          height={200}
          dismissOnTouchOutside={false}
          visible={true}
          >
          <View style={{flex:1}}>
             {this._renderPopupView()}
          </View>
        </PopupDialog>

      </View>
    )
  }

}
