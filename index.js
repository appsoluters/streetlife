import React from 'react';
import {AppRegistry} from 'react-native';
import App from './streetLife/App';
import {name as appName} from './app.json';

const StreetLife = () => (
      <App />
)

AppRegistry.registerComponent(appName, () => StreetLife);
